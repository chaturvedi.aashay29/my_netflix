const SET_ROLE = "set_role";
const GET_CONTENT = "get_content";
const UPDATE_WATCHLIST = "update_watchlist";

export { SET_ROLE, GET_CONTENT, UPDATE_WATCHLIST };
