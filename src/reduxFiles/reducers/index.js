import { combineReducers } from "redux";
import { loginReducer as login } from "./loginReducer";
import { contentReducer as content } from "./contentReducer";

export default combineReducers({
  login,
  content,
});
