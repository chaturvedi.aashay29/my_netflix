// import MyLogger from "../../utils/MyLogger";
import { GET_CONTENT, UPDATE_WATCHLIST } from "../constants/action-types";

const INITIAL_STATE = {
  allContent: [],
  watchlist: [],
};

export const contentReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_CONTENT:
      return {
        ...state,
        allContent: action.payload,
      };
    case UPDATE_WATCHLIST:
      return {
        ...state,
        watchlist: action.payload,
      };
    default:
      return state;
  }
};
