// import MyLogger from "../../utils/MyLogger";
import { SET_ROLE } from "../constants/action-types";

const INITIAL_STATE = {
  role: "admin",
};

export const loginReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_ROLE:
      return {
        ...state,
        role: action.payload,
      };
    default:
      return state;
  }
};
