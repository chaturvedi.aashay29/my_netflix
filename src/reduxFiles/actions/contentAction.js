import { GET_CONTENT, UPDATE_WATCHLIST } from "../constants/action-types";
import { api } from "../../utils/apiConstants";
import axios from "axios";

export const getAllContent = () => (dispatch, getState) => {
  axios
    .get(api.baseUrl + api.getAllContent)
    .then((res) => {
      dispatch({
        type: GET_CONTENT,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error = ", err);
    });
};

export const getWatchlist = () => (dispatch, getState) => {
  axios
    .get(api.baseUrl + api.getWatchlist, { params: { userName: "Aashay" } })
    .then((res) => {
      dispatch({
        type: UPDATE_WATCHLIST,
        payload: res.data.watchlist,
      });
    })
    .catch((err) => {
      console.log("error = ", err);
    });
};

export const setInWatchList = (id) => (dispatch, getState) => {
  let data = { id: id, userName: "Aashay" };
  axios
    .post(api.baseUrl + api.addToWatchList, data)
    .then((res) => {
      if (res.status == 200) {
        dispatch({
          type: UPDATE_WATCHLIST,
          payload: res.data.watchlist,
        });
      } else {
        dispatch({
          type: UPDATE_WATCHLIST,
          payload: [],
        });
      }
    })
    .catch((err) => {
      console.log("error = ", err);
    });
};

export const removeFromWatchList = (id) => (dispatch, getState) => {
  let data = { id: id, userName: "Aashay" };
  axios
    .post(api.baseUrl + api.removeFromWatchList, data)
    .then((res) => {
      if (res.status == 200) {
        dispatch({
          type: UPDATE_WATCHLIST,
          payload: res.data.watchlist,
        });
      } else {
        dispatch({
          type: UPDATE_WATCHLIST,
          payload: [],
        });
      }
    })
    .catch((err) => {
      console.log("error = ", err);
    });
};
