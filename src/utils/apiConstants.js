export const api = {
  baseUrl: "http://localhost:9000/",
  getAllContent: "getAllContent",
  addToWatchList: "addToWatchList",
  getWatchlist: "getWatchlist",
  removeFromWatchList: "removeFromWatchList",
};
