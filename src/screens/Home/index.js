import React, { useEffect, useState } from "react";
import "./index.scss";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import CardItem from "../../components/cardItem";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import { getAllContent, getWatchlist } from "../../reduxFiles/actions";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 8,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 6,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 4,
  },
};

const CustomLeftArrow = ({ onClick, ...rest }) => {
  const {
    onMove,
    carouselState: { currentSlide, deviceType },
  } = rest;
  return (
    <div
      className="d-flex align-items-center bg"
      style={{
        position: "absolute",
        height: "100%",
      }}
      onClick={onClick}
    >
      <ChevronLeftIcon
        className="overlay"
        style={{
          color: "red",
          height: "35px",
          width: "40px",
        }}
      />
    </div>
  );
};

const CustomRightArrow = ({ onClick, ...rest }) => {
  const {
    onMove,
    carouselState: { currentSlide, deviceType },
  } = rest;
  return (
    <div
      className="d-flex align-items-center bg"
      style={{
        position: "absolute",
        height: "100%",
        right: 0,
      }}
      onClick={onClick}
    >
      <ChevronRightIcon
        className="overlay"
        style={{
          color: "red",
          height: "35px",
          width: "40px",
        }}
      />
    </div>
  );
};

const Home = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const allContent = useSelector((state) => state.content.allContent);
  const watchlist = useSelector((state) => state.content.watchlist);

  const [search, setSearch] = useState("");

  useEffect(() => {
    dispatch(getAllContent());
    dispatch(getWatchlist());
  }, []);

  return (
    <div>
      <div className="m-3 d-flex justify-content-end">
        <span style={{ marginRight: "10px" }}>Search</span>
        <input
          style={{ paddingLeft: "5px" }}
          onChange={(e) => {
            setSearch(e.target.value);
          }}
          value={search}
        />
      </div>

      <span className="font-weight-bold ml-3 font-color-gray600">
        Your List
      </span>
      <div className="mb-3">
        <Carousel
          swipeable={false}
          draggable={false}
          showDots={false}
          responsive={responsive}
          infinite={false}
          autoPlay={false}
          customLeftArrow={<CustomLeftArrow />}
          customRightArrow={<CustomRightArrow />}
          containerClass="carousel-container"
          dotListClass="custom-dot-list-style"
          itemClass="carousel-item-padding-40-px"
        >
          {allContent.map((item, index) => {
            if (watchlist.includes(item._id)) {
              return (
                <CardItem
                  key={index}
                  name={item.name}
                  year={item.yearOfRelease}
                  cast={item.cast}
                  imageUrl={item.imageUrl}
                  id={item._id}
                  addedToWatchlist={true}
                  onClick={() => history.push("/detail")}
                />
              );
            }
          })}
        </Carousel>
      </div>
      <span className="font-weight-bold ml-3 font-color-gray600 ">All</span>
      <div className="">
        {allContent && allContent.length > 0 ? (
          <div>
            {search == ""
              ? allContent.map((item, index) => {
                  return (
                    <CardItem
                      key={index}
                      name={item.name}
                      year={item.yearOfRelease}
                      cast={item.cast}
                      imageUrl={item.imageUrl}
                      id={item._id}
                      addedToWatchlist={false}
                    />
                  );
                })
              : allContent.map((item, index) => {
                  if (
                    item.name.includes(search) ||
                    item.cast.includes(search)
                  ) {
                    return (
                      <CardItem
                        key={index}
                        name={item.name}
                        year={item.yearOfRelease}
                        cast={item.cast}
                        imageUrl={item.imageUrl}
                        id={item._id}
                        addedToWatchlist={false}
                      />
                    );
                  }
                })}
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default Home;
