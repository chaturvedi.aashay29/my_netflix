import React from "react";
import Card from "@material-ui/core/Card";
import StarIcon from "@material-ui/icons/Star";
import StarOutlineIcon from "@material-ui/icons/StarOutline";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import CheckIcon from "@material-ui/icons/Check";
import { Image, Row, Col } from "react-bootstrap";
import "./index.css";
import { useDispatch } from "react-redux";
import { setInWatchList, removeFromWatchList } from "../../reduxFiles/actions";
import { useHistory } from "react-router-dom";

const CardItem = (props) => {
  const history = useHistory();

  const { name, year, cast, imageUrl, id, addedToWatchlist } = props;
  const dispatch = useDispatch();

  return (
    <Card
      className="my-3 mx-3 pt-2"
      style={{
        minWidth: "200px",
        maxWidth: "350px",
        display: "inline-block",
      }}
      onClick={() => history.push("/detail")}
    >
      <Row className="mt-3">
        <Image
          className="mx-auto"
          style={{ height: 170, width: 170 }}
          src={imageUrl}
        />
      </Row>
      <Row className="justify-content-center mt-1 mb-2">
        <div className="flex-column">
          <span className="d-flex justify-content-center font-size-medium font-weight-bold font-color-black">
            {name}
          </span>
          <span
            style={{ paddingLeft: "15px", paddingRight: "15px" }}
            className="d-flex justify-content-center font-size-medium font-color-black"
          >
            {cast}
          </span>
        </div>
      </Row>
      <span
        style={{ paddingLeft: "15px" }}
        className="ml-2 mt-2 font-size-medium font-color-black"
      >
        {year}
      </span>
      <Row className="m-0 background-color-gray-600">
        <Col className="d-flex py-1  justify-content-center">
          {addedToWatchlist ? (
            <CheckIcon onClick={() => dispatch(removeFromWatchList(id))} />
          ) : (
            <AddCircleOutlineIcon
              onClick={() => dispatch(setInWatchList(id))}
            />
          )}
        </Col>
        <Col className="d-flex py-1 justify-content-center">
          <StarIcon />
        </Col>
      </Row>
    </Card>
  );
};

export default CardItem;
