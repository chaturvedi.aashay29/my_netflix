import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import LoginPage from "./screens/Login";
import RegistrationPage from "./screens/Register";
import HomePage from "./screens/Home";
import DetailPage from "./screens/Detail";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/login" component={LoginPage}></Route>
          <Route exact path="/" component={HomePage}></Route>
          <Route exact path="/detail" component={DetailPage}></Route>
          <Route exact path="/register" component={RegistrationPage}></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
